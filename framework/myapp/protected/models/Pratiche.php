<?php

/**
 * This is the model class for table "pratiche".
 *
 * The followings are the available columns in table 'pratiche':
 * @property integer $id
 * @property string $id_pratiche
 * @property string $data_creazione
 * @property string $stato_pratica
 * @property string $note
 * @property integer $id_cliente
 *
 * The followings are the available model relations:
 * @property Clienti $idCliente
 */
class Pratiche extends CActiveRecord
{
	public $codice_fiscale;
	public $nome;
	public $cognome;
	public $file_csv;
	
	public function fb($what){
	  echo Yii::trace(CVarDumper::dumpAsString($what),'vardump');
	}
		
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pratiche';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_pratiche, data_creazione, stato_pratica, note, id_cliente', 'required'),
			array('id_cliente', 'numerical', 'integerOnly'=>true),
			array('stato_pratica', 'length', 'max'=>5),
			// The following rule is used by search().
			array('id_pratiche, codice_fiscale', 'safe', 'on'=>'search'),
		);
	}
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idCliente' => array(self::BELONGS_TO, 'Clienti', 'id_cliente'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_pratiche' => 'Id Pratiche',
			'data_creazione' => 'Data Creazione',
			'stato_pratica' => 'Stato Pratica',
			'note' => 'Note',
			'id_cliente' => 'Id Cliente',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	 	
	
	public function search()
	{
		$criteria=new CDbCriteria;
		
		$criteria->select=array('id_pratiche','data_creazione','stato_pratica','nome','cognome');
		
		$criteria->join='LEFT JOIN clienti ON id_cliente = clienti.id';
		
		$criteria->compare('id_pratiche',$this->id_pratiche,true);
		
		$criteria->compare('codice_fiscale',$this->codice_fiscale,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function searchAll()
	{
		$criteria=new CDbCriteria;
		
		$criteria->select=array('id','id_pratiche','data_creazione','stato_pratica','note','id_cliente');

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Pratiche the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
