<?php

class PraticheController extends Controller
{

	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(''),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('admin'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('delete','export','exportFile','exportDb','exportAll','import','create'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Pratiche;

		if(isset($_POST['Pratiche']))
		{
			$model->attributes=$_POST['Pratiche'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		if(isset($_POST['Pratiche']))
		{
			$model->attributes=$_POST['Pratiche'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Pratiche');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}
	/**
	 *Creates file .csv 
	 */
	
	public function actionExport()
	{
		$fp = fopen('php://temp', 'w');

		/* 
		 * Write a header of csv file
		 */
		 
		$headers = array('id_pratiche','data_creazione','stato_pratica','nome','cognome');
		
		$row = array();
		
		foreach($headers as $header) {
			$row[] = $header;
		}
		
		fputcsv($fp,$row,";");

		/*
		 * Init dataProvider for first page
		 */
		 
		$model=new Pratiche('search');
		
		$model->unsetAttributes(); 
		
		if(isset($_GET['Pratiche'])) {
			$model->attributes=$_GET['Pratiche'];
		}
		
		$dp = $model->search();
		
		$dp->setPagination(false);

		$models = $dp->getData();
		
		foreach($models as $model) {
			
			$row = array();
			
			foreach($headers as $head) {
				$row[] = CHtml::value($model,$head);
			}
			
			fputcsv($fp,$row,";");
		}

		/*
		 * save csv content to a Session
		 */
		 
		rewind($fp);
		
		Yii::app()->user->setState('export',stream_get_contents($fp));
		
		fclose($fp);
	}	

	public function actionExportFile()
	{
		Yii::app()->request->sendFile('export.csv',Yii::app()->user->getState('export'));
		
		Yii::app()->user->clearState('export');
	}
		
	public function actionExportAll()
	{	
		$file = Yii::getPathOfAlias('webroot.protected.exportdb').'/exportAll.csv';
		
		if(file_exists($file))
			unlink($file);
		
		$fp = fopen($file, 'w');
		
		for($i=0;$i<=1;$i++){
			if($i==0)
				$headers = array('id','nome','cognome','codice_fiscale','note');
			else
				$headers = array('id','id_pratiche','data_creazione','stato_pratica','note','id_cliente');
			
			$row = array();
			
			foreach($headers as $header) {
				$row[] = $header;
			}
			
			fputcsv($fp,$row,";");
			
			if($i==0){
				$model=new Clienti('search');
			}else{
				$model=new Pratiche('searchAll');
			}
						
			
			$dp = $model->searchAll();
			
			$dp->setPagination(false);

			$models = $dp->getData();
			
			foreach($models as $model) {
				
				$row = array();
				
				foreach($headers as $head) {
					$row[] = CHtml::value($model,$head);
				}
				
				fputcsv($fp,$row,";");
			}
			$newTable=array('new_table');
			fputcsv($fp,$newTable,";");
		}
		
		fclose($fp);
		
		$this->downloadFile($file);
	}
	
	
	public function downloadFile($fullpath){
	  if(!empty($fullpath)){ 
		  header("Content-type:application/csv"); 
		  
		  header('Content-Disposition: attachment; filename="'.basename($fullpath).'"'); 
		  
		  header('Content-Length: ' . filesize($fullpath));
		  
		  readfile($fullpath);
		  
		  Yii::app()->end();
	  }
	}
	
	public function actionImport()
	{
		$model=new Pratiche;

		if(isset($_POST['Pratiche']) AND $_FILES['Pratiche']['name']['file_csv']!='')
		{	

			$model->file_csv=CUploadedFile::getInstance($model,'file_csv');
			
			$file=Yii::getPathOfAlias('webroot.protected.exportdb').'/fileToImport.csv';
			
			if(file_exists($file))
				unlink($file);
			
			$model->file_csv->saveAs($file);
			
			$row = 1;
			
			$flag=true;
			
			if (($handle = fopen($file, "r")) !== FALSE) {

				while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
					
					$num = count($data);
				
					if($row!=1){
						if($data[0]!='new_table'){ 

							if($flag==true){
								
								$model=new Clienti();
								
								$model->unsetAttributes(); 
								
								$headers = array('id','nome','cognome','codice_fiscale','note');
								
								for ($c=0; $c < $num; $c++) {
								
									$dataToSave[$headers[$c]]=$data[$c];
								}

								$model->attributes=$dataToSave;

								$model->save();
								
							}else{
								
								$model=new Pratiche();
								
								$model->unsetAttributes(); 
								
								$headers = array('id','id_pratiche','data_creazione','stato_pratica','note','id_cliente');
								
								for ($c=0; $c < $num; $c++) {
								
									$dataToSave[$headers[$c]]=$data[$c];
								}

								$model->attributes=$dataToSave;
							
								$model->save();
								
							}
						}else{
							$flag=false;
						}
						$dataToSave=array();
					}
					$row++;
				}
				
			}
			  
			fclose($handle);
		
		}		
		$this->render('import', array('model'=>$model));
	}

	/**
	 * Manages all models.
	 */	 
	public function actionAdmin()
	{
		if(Yii::app()->request->getParam('export')) {
			
			$this->actionExport();
			
			Yii::app()->end();
		}
		
		$model=new Pratiche('search');
		
		$model->unsetAttributes();  // clear any default values
		
		if(isset($_GET['Pratiche']))
			$model->attributes=$_GET['Pratiche'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Pratiche the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Pratiche::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Pratiche $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='pratiche-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	/*Debug for variables*/
	public function fb($what){
	  echo Yii::trace(CVarDumper::dumpAsString($what),'vardump');
	}
}
