<?php 

$this->breadcrumbs=array(
	'Manage',
);
?>
<h2>Import .CSV file for Pratiche and Clienti</h2>

<?php
$form = $this->beginWidget(
	'CActiveForm',
	array(
		'id' => 'upload-form',
		'enableAjaxValidation' => false,
		'htmlOptions' => array('enctype' => 'multipart/form-data'),
	)
);

echo $form->labelEx($model, 'file_csv');
echo $form->fileField($model, 'file_csv');
echo $form->error($model, 'file_csv');

echo CHtml::submitButton('Submit');
$this->endWidget();
?>