<?php
/* @var $this PraticheController */
/* @var $model Pratiche */

$this->breadcrumbs=array(
	'Manage',
);

$restriction=Yii::app()->user->restriction;
	
if($restriction=='0'){
	$this->menu=array(
	array('label'=>'Importa File CSV', 'url'=>array('import')),);
}
	
Yii::app()->clientScript->registerScript('search', "
	$('.search-form form').submit(function(){
		$('#pratiche-grid').yiiGridView('update', {
			data: $(this).serialize()
		});
		return false;
	});
");

Yii::app()->clientScript->registerScript("esporta", "
	$('#export-button').on('click',function() {
	$.fn.yiiGridView.export();
	});
	$.fn.yiiGridView.export = function() {
	$.fn.yiiGridView.update('pratiche-grid',{ 
		success: function() {
			$('#pratiche-grid').removeClass('grid-view-loading');
			window.location = '". $this->createUrl('exportFile')  . "';
		},
		data: $('.search-form form').serialize() + '&export=true'
	});
	}
");


?>

<h1>Manage Pratiche</h1>

<div class="search-form">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php 
	
	if($restriction=='0'){
		echo CHtml::button('Export', array('id'=>'export-button'));
		echo CHtml::button('Export All', array('onclick' => 'js:document.location.href="index.php?r=pratiche/exportAll"'));
	}


$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'pratiche-grid',
	'dataProvider'=>$model->search(),
	'columns'=>array(
		'id_pratiche',
		'data_creazione',
		'stato_pratica',
		array(
            'header'=>'Nome e Cognome',
            'value'=>'$data->nome." ".$data->cognome',
		),
		
	),
)); ?>


