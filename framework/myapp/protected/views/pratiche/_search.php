<?php
/* @var $this PraticheController */
/* @var $model Pratiche */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
));
 ?>

	<div class="row">
		<?php echo $form->label($model,'id_pratiche'); ?>
		<?php echo $form->textField($model,'id_pratiche',array('size'=>0,'maxlength'=>50)); ?>
	</div>


	<div class="row">
		<?php echo $form->label($model,'codice_fiscale'); ?>
		<?php echo $form->textField($model,'codice_fiscale',array('size'=>0,'maxlength'=>50)); ?>
	</div>


	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->