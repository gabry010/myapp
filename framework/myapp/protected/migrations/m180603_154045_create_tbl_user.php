<?php

class m180603_154045_create_tbl_user extends CDbMigration
{
	public function up()
	{
		$this->createTable('tbl_user', array(
		'id'=>'pk AUTO_INCREMENT',
		'username'=>'varchar(128) NOT NULL',
		'password'=>'varchar(128) NOT NULL',
		'restriction'=>'tinyint(128) NOT NULL',
		));
		
		$this->insert('tbl_user',array(
			'username'=>'admin',
			'password'=>'pass1',
			'restriction'=>0,
		));
		
		$this->insert('tbl_user',array(
			'username'=>'user',
			'password'=>'pass2',
			'restriction'=>1,
		));
		
	}

	public function down()
	{
		echo "m180603_154045_create_tbl_user does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}