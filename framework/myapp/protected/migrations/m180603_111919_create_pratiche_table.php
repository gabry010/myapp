<?php

class m180603_111919_create_pratiche_table extends CDbMigration
{
	public function up()
	{
				
		$this->createTable('clienti', array(
		'id'=>'pk AUTO_INCREMENT',
		'nome'=>'varchar(128) NOT NULL',
		'cognome'=>'varchar(128) NOT NULL',
		'codice_fiscale'=>'varchar(128) NOT NULL',
		'note'=>'text NOT NULL',
		));
		
		$this->createTable('pratiche', array(
		'id'=>'pk AUTO_INCREMENT',
		'id_pratiche'=>'varchar(128) NOT NULL',
		'data_creazione'=>'datetime NOT NULL',
		'stato_pratica'=>'enum("open","close") NOT NULL',
		'note'=>'text NOT NULL',
		'id_cliente'=>'INT NOT NULL',
		));
		
		$this->addForeignKey('fk_id_cliente', 'pratiche', 'id_cliente',
		'clienti', 'id', 'NO ACTION', 'NO ACTION');
		
		$this->insert('clienti',array(
			'nome'=>'Gabriele',
			'cognome'=>'Russo',
			'codice_fiscale'=>'RSSGRL94R04C351P',
			'note'=>'Developer',
		));
		
		$this->insert('clienti',array(
			'nome'=>'Patrick',
			'cognome'=>'Facco',
			'codice_fiscale'=>'PTRFCC00R04F748',
			'note'=>'Manager',
		));
		
		$this->insert('clienti',array(
			'nome'=>'Mario',
			'cognome'=>'Rossi',
			'codice_fiscale'=>'MRRSS8554T0315',
			'note'=>'Cliente',
		));

	}

	public function down()
	{
		echo "m180603_111919_create_pratiche_table does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}